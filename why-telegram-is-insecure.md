TL;DR

Why Telegram is insecure? The following points make Telegram less secure than other apps.

# Telegram's end-to-end encryption is not turned on by default. 

Yes, it has E2E "Secret chats", but you need to manually activate it. Apps like Threema or Signal are better options as everything is end-to-end encrypted by default.

# Telegram use phone number

Telegram links an account to a telephone number. The messenger verifies that the phone number is accessible to the user when they register their account (via an authentication code sent over SMS, or via a call.) For an attacker with access to the telephone company systems (e.g. SS7 injection, or a national telephone operator) hijacking the verification code for the account is straightforward. Simply redirect the SMS/calls to the number to a location that is under attack control/visibility.

This attack has been used a number of times in the wild. At first there were only anecdotal reports from Iran of Telegram accounts being hijacked. Then an account hijacking in Russia was well documented and made public. Clearly, at least some nation states are using this technique to take hijack accounts.

Telegram added an additional security feature (but not required) to address this attack - *a password*. If a password is set for an account, then both the authentication sent to the phone number and the password are required to access an account on a new device.

# Cloud Messaging

All non end to end encrypted chats are automatically backed up to the Telegram servers. When the user accesses their account from another device, their entire chat history is available to them. This is a security nightmare. It means that an account compromise exposes historical data to the adversary, not just for the duration of the compromise. Storing sensitive data is a dangerous play, always.

# Error Prone Defaults

Messages are not end to end encrypted by default. There is no way to opportunistically encrypt an existing session. Instead users must get select a "New Secret Chat" and then start chatting. This is error prone. The most likely case is that people will make the mistake of clicking on the contact they wish to speak to rather than going through the multi step process of setting up a "Secret Chat". Tools that allow for mistakes encourage operational errors. If it is possible, it will happen.

# Contact Theft

When registering an account with Telegram, the app helpfully uploads the entire Contacts database to Telegram's servers (optional on iOS). This allows Telegram to build a huge social network map of all the users and how they know each other. It is extremely difficult to remain anonymous while using Telegram because the social network of everyone you communicate with is known to them (and whomever has pwned their servers).

Contact books are extremely valuable information. We know that the NSA went to great lengths to steal them from instant messenger services. On mobile the contact lists are even more important because they are very frequently linked to real world identities.

# Voluminous Metadata

Anything using a mobile phone exposes a wide range of metadata. In addition to all the notification flows through Apple and Google's messaging services, there is the IP traffic flows to/from those servers, and the data on the Telegram servers. If I were a gambling man, I’d bet those servers have been compromised by nation state intelligence services and all that data is being dumped regularly.

This metadata would expose who talked with who, at what time, where they were located (via IP address), how much was said, etc. There is a huge amount of information in those flows that would more than compensate for lacking access to the content (even if, big assumption, the crypto is solid).

# Telegram can see what you are writing still if you are using "End-to-end" chats

Telegram provides a feature called "Links previews" that's available and on by default in not encrypted chats, anyways if you use a "Secret chat", Telegram app ask to you if you want to use "links previews" adversiting that it previews are done in the server side. How can they know what links are you writing? Can they read your messages still if you are using "Secret chats"? (Not sure but is a edge case).

# Local storage not encrypted

The local storage is encrypted *only* if you use "Secrets chats".

# Telegram uses a home grown encrption protocol called MTProto.

Telegram's security is built around their home spun MTProto protocol. We all know that the first rule of Cryptography is "Don't Roll Your Own Crypto". Especially if you aren't trained cryptographers. Which the Telegram people most certainly aren't. 

From a high-level point of view, Telegram allows two devices to exchange a long term key using Diffie-Hellman key exchange. Afterwards, the two devices can use
this key to exchange encrypted messages using a symmetric encryption scheme known as MTProto. In a nutshell MTProto is a combination of:

1. A lesser known mode of operation, namely Infinite Garble Extension (IGE).
2. A short term key derivation mechanism.
3. An integrity check on the plaintext.

Here is an abstract description of MTProto:

# **MTProto Encryption**

Long-Term key: The sender and the receiver share a long term-key *K* of length λ. (In MTProto λ = 2048 bits and the key is obtained by running the Diffie-Hellman Key-Exchange over Z∗p).

Payload: The payload x = (aux,rnd,msg,pad) is obtained by concatenating:

1. Some auxiliary information *aux*.
2. A random value *rnd* (in MTProto this has length 128 bits).
3. The message *msg*.
4. Some  arbitrary  padding *pad* such  that |x| mod B =  0,  where B is  the block length; For technical reasons this is never more than 96 bits (and not 120 bits as described by the official documentation).

Authentication Tag: MTProto employs a hash function:

Tag: {0,1}^∗ → {0,1}^h

For authentication purposes. This is used to compute:

tag = Tag(aux,rnd,msg)

(crucially, *pad* is not part of the input of the authentication tag). In MTProto, Tag = SHA-1 and h = 128 bits.

Short-Term Key Derivation: MTProto employs a hash function. KDF: {0,1}^λ+h → {0,1}^κ+2B

for key derivation purposes. This is used to compute: (k,x0,y0) = KDF(K,tag)

of length (κ,B,B) respectively. In MTProto the output of KDF is obtained by (a permutation of) the output of four SHA-1 invocations, which in turn take as input (a permutation of) the tag and (different) portions of the long term key K.

**IGE Encryption**: MTProto  employs  an  efficiently  invertible  pseudorandom permutation 

F,F−1:{0,1}^κ×{0,1}^B → {0,1}B

to implement the IGE mode of operation. Let

x1,...,xy be the y blocks of the payload, each of length B, then IGE computes

yi=Fk(xi ⊕ yi− 1) ⊕ xi−1 (where x0,y0 are defined in the previous step). In MTProto

F=AES with κ = 256 bits and B = 128 bits.

**Resulting Ciphertext**: The output of MTProto is c= (tag,y1,...,yz)

# **MTProto Decryption**

**Input Parsing**: The ciphertext c is parsed as c = (tag,y1,...,yz) 

**Short-Term Key Recomputation**: The short-term key and the initialization blocks are recomputed as (k,x0,y0) = KDF(K,tag)

**IGE Decryption**: The payload is recovered by computing: xi=yi−1 ⊕ F−1k(yi ⊕ xi−1) and (x1,...,xy) is parsed as (aux,rnd,msg,pad).

**Tag Verification**: The decryption checks if tag (?=) Tag(aux,rnd,msg) and, if so, outputs msg (or if not otherwise).

Interestingly, if a Telegram client receives an ill-formed ciphertext, it simply drops the message and does not notify the sender about the error. This implies that it is very hard for a network attacker to detect whether a decryption was performed  successfully  or  not  (and  therefore  it  does  not  seem  possible  to  run a Bleichenbacher-style attack [ciphertext attacks against protocols based on
the RSA encryption standard PKCS]). However, we cannot exclude that an attacker which sits on the same client and shares resources (such as memory, CPU, etc.) might be able to detect if (and how) decryption fails using other channels.

# **MTProto is not IND-CCA secure**

Here I briefly present the two attacks that show that MTProto is not IND-CCA secure. We assume the reader to be familiar with the notion of IND-CCA security (If not, see https://en.wikipedia.org/wiki/Ciphertext_indistinguishability). More details on the attacks (and experimental validations) can be found in [http://cs.au.dk/~jakjak/master-thesis.pdf].

Once again, I stress that the attacks are only of theoretical nature and we do  not  see  a  way  of  turning  them  into  full-plaintext  recovery.  Yet,  we  believe that these attacks are yet another proof [https://eprint.iacr.org/2015/428.pdf] that designing your own crypto rarely is a good idea.

**Attack #1: Padding-Length Extension**

Given a ciphertext:

c= (tag,y1,...,yz) which encrypts a payload x= (aux,rnd,msg,pad)

the attacker picks a random block

r ← {0,1}B and outputs

cz= (tag,y1,...,yz,r)

When this is run via the decryption process, the resulting payload is

xz= (aux,rnd,msg,padz) where padz=  (pad,pad∗)  where:  

1) pad∗ is  randomly  distributed.
2) The length  of |padz| is  larger  than  the  block  length B.  However,  since  the  length of  the padz is  not  checked  during  decryption, and padz is  not  an input  to  the authentication function Tag, decryption outputs msg with probability 1.

**Attack #2: Last Block Substitution**

Given a ciphertext

c= (tag,y1,...,yz) which encrypts a payload x= (aux,rnd,msg,pad)

the attacker picks a random block

r ← {0,1}^B and outputs

c′= (tag,y1,...,y`−1,r)

When this is run via the decryption process, the resulting payload is

x′= (aux,rnd,msg′,pad′)  When this is run via the decryption process, the resulting payload is

x′= (aux,rnd,msg′,pad′) 

Let P=|pad| be the length of the original pad, then it holds that the last B−P bits of msg′ are randomly distributed (due to the use of the PRF in the IGE decryption). Hence we have msg′ = msg with probability 2P−B, and in this case the verification of the authentication tag succeeds and the decryption outputs msg.  According  to  the  official  description  of  MTProto  the  padding  length  is between 0 and 120 bits, which means that in the best case scenario the above attack  succeeds  with  probability  2^−8.  However  experimental  validation  shows that this is not the case and the length of the padding is in fact between 0 and 96 bits (this is due to the way the objects are serialized in Java). So I conclude that in the best case this attack succeeds with probability 2^−32 which, while still being too high to satisfy the definition of security, makes this attack more cumbersome than expected.

# **Conclusion**

The symmetric encryption scheme used by Telegram, fails to achieve desirable notions of security such as indistinguishability  under  chosen ciphertext  attack or authenticated  encryption. While the first attack can be mitigated by applying a patch which does not affect backward compatibility with unpatched (but honest) senders, the second one can only be patched in a way that only allows communication between patched clients. Therefore it is fair to ask whether the next version of Telegram should use a patched version of MTProto or a better studied mode of operation which provably implements authenticated encryption.

I believe that the second option is the only sensible choice since while a patched version of MTProto would not be susceptible to the attacks described in this note, this gives no guarantees that no other attacks exist. On the other hand well studied authenticated encryption schemes offer much stronger security guarantees. In addition, MTProto does not seem to be more efficient than the many available options for authenticated  encryption such as e.g., encrypt-then-MAC using AES in counter mode (which can be parallelized better than IGE) together with e.g., HMAC.

**So, no. Telegram is by no means secure. For commonly accepted definitions of secure, not the one Telegram made up.**

**The safest way to use Telegram would be not to. However, if you have no other choice, the best approach would be to use a clean burner phone to communicate with another clean burner phone. Change them regularly.**

# Use instead of Telegram

If you want to use secure and private apps, I recommend:

Signal Private Messenger: https://signal.org

Threema: https://threema.ch

If you want contact me on Threema: https://three.ma/736WU8VV or in XMPP (only with OMEMO encrytion): edu4rdshl@conversations.im or follow me on Twitter: https://twitter.com/edu4rdshl
